import React from 'react';
import "../views/users/users";
import Home from "../views/home/home";
import UserList from "../views/users/users";
import CreateEditUser from "../views/users/createEditUser";

const routes = [
    {
        name: "Home",
        path: '/home',
        component: <Home/>
    },
    {
        name: 'UserList',
        path: '/users',
        component: <UserList/>
    },
    {
        name: 'CreateEditUser',
        path: '/users/edit/:id',
        component: <CreateEditUser/>
    },
    {
        name: 'CreateEditUser',
        path: '/users/new',
        component: <CreateEditUser/>
    },
    {
        name: "",
        path:"/",
        component: <Home/>
    }
];

export function getRoutes(){
    return routes;
}