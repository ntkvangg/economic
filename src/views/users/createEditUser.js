import React, {useEffect, useMemo, useState} from "react";
import {Link, useParams, useNavigate} from "react-router-dom";
import validator from "validator"
import {createUser, getCurrentUser, updateUser} from "../../services/Users.service";
import {useTranslation} from "react-i18next"
import {toast} from "react-toastify";
import i18next from "../../i18next";


function CreateEditUser(){

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [description, setDescription] = useState("");
    const [isValidEmail, setIsValidEmail] = useState(false);
    const [isChangeData, setChangeData] = useState(false);

    const {id} = useParams();
    const navigate = useNavigate();
    const { t, i18n } = useTranslation();



    useEffect(()=> {
        if(id){
            getCurrentUser(id).then(response=>{
                const data = response.data;
                setFirstName(data.firstName);
                setLastName(data.lastName);
                setEmail(data.email);
                setDescription(data.description);
                setIsValidEmail(validator.isEmail(data.email));

            });
        }

    }, []);



    const isDisableCreateBtn= ()=>{
        return firstName && lastName && isValidEmail;
    };

    const isDisableSaveBtn = ()=>{
      return isDisableCreateBtn() && isChangeData;
    };



    const onChangeFirstName = (e)=>{
        setFirstName(e.target.value);
        setChangeData(true);
    };

    const onChangeLastName = (e)=>{
        setLastName(e.target.value);
        setChangeData(true);
    };

    const onChangeEmail = (e)=>{
        setEmail(e.target.value);
        setIsValidEmail(validator.isEmail(email) && !!email);
        setChangeData(true);
    };

    const onChangeMessage = (e)=>{
        setDescription(e.target.value);
        setChangeData(true);
    };

    const createEditUser = ()=>{
        const payload = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            message: description
        };
        if(id){
            updateUser(id,payload).then(()=>{
                toast.success(t("user_updateSuccessfully"));
                navigate('/users');
            })
        }else {
            createUser(payload).then(()=>{
                toast.success(t("user_createSuccessfully"));
                navigate('/users');
            })
        }

    };



        return (
            <div className="create-edit-user">
                <div className="card border-0 p-3">
                    {t("user_addNewTitle")}
                </div>
                <div className="card mt-5 me-4 ms-4">
                    <div className="card-header">
                        {/*<h6 className="card-title">{t("user_basicInformation")}</h6>*/}
                        <h6 className="card-title">{i18next.t("user_basicInformation")}</h6>

                    </div>
                    <div className="card-body">

                        <div className="form-group row">
                            <div className="col-lg-6">
                                <label className="form-label text-lg-start">First Name *</label>
                                <div className="input-group">
                                    <span className="input-group-text" id="basic-addon1"><i className="fa fa-pencil"></i></span>
                                    <input type="text" className="form-control" placeholder="Enter a first name" onChange={onChangeFirstName} value={firstName}
                                           aria-label="Username" aria-describedby="basic-addon1"/>
                                </div>
                                { !!firstName ? null: <div className="text-danger text-invalid">This field is required.</div>}
                            </div>
                            <div className="col-lg-6">
                                <label className="form-label text-lg-start">Last Name *</label>
                                <div className="input-group">
                                    <span className="input-group-text" id="basic-addon1"><i className="fa fa-pencil"></i></span>
                                    <input type="text" className="form-control" placeholder="Enter a last name" onChange={onChangeLastName} value={lastName}
                                           aria-label="Username" aria-describedby="basic-addon1"/>
                                </div>
                                { !!lastName ? null : <div className="text-danger text-invalid">This field is required.</div> }
                            </div>

                        </div>

                        <div className="form-group row">
                            <div className="col-lg-6">
                                <label className="form-label text-lg-start">Email *</label>
                                <div className="input-group">
                                    <span className="input-group-text" id="basic-addon1">@</span>
                                    <input type="text" className="form-control" placeholder="Enter a email" onChange={onChangeEmail} value={email}
                                           aria-label="Username" aria-describedby="basic-addon1"/>
                                </div>
                                { isValidEmail ?  null : <div className="text-danger text-invalid">Please enter a valid email.</div>}
                                { !!email ?  null : <div className="text-danger text-invalid">This field is required.</div>}
                            </div>

                        </div>

                        <div className="form-group row">
                                <div className="col-lg-6">
                                    <label className="form-label">Message</label>
                                    <textarea className="form-control" onChange={onChangeMessage} value={description}></textarea>
                                </div>

                            </div>
                    </div>

                    <div className="card-footer">
                        <div className="d-flex justify-content-end">
                            <Link className="btn btn-secondary me-2" to="/users">
                                Cancel
                            </Link>

                            <button className="btn btn-primary" disabled={id ? !isDisableSaveBtn() : !isDisableCreateBtn()} onClick={()=>{createEditUser()}}>
                                {id ? t("common_save") : t("common_create") }
                            </button>
                        </div>

                    </div>

                </div>
            </div>
        )
}
export default CreateEditUser;
