import React, {useEffect, useState} from "react";
import AgGridTable from "../../components/agGridTable/AgGridTable";
import "../users/users.css";
import {getUserList, deleteUser} from "../../services/Users.service";
import {NavLink, useNavigate} from 'react-router-dom';
import useModal from "../../components/modal/useModal";
import ModalConfirmation from "../../components/modal/confirmationModal";
import {toast} from "react-toastify";
import { useTranslation } from 'react-i18next';





const ActionsCellRenderer = (props)=>{

    const {t, i18n} = useTranslation();
    const {isShowing, toggle} = useModal();
    const rowNode = props.data;
    //Hook
    const navigate = useNavigate();

    useEffect(()=>{

    }, []);

    //Function
    const onEdit = () => {
        navigate(`/users/edit/${rowNode.id}`);
    };

    const onDelete = ()=>{
        toggle();
    };

    const onDeleteUser = () =>{
        deleteUser(props.data.id).then(()=>{
            toast.success(i18n.t("user_deleteSuccessfully"));
            getUserList().then((response)=>{
                props.context.methodFromParent(response.data);
            })
        });
    };

    const config = {
        message: "user_deleteConfirmationText",
        variableI18n: rowNode.email,
        title: "Confirmation",
        onClick: onDeleteUser
    };



    //render html
    return (
        <div className="d-inline">
            <ModalConfirmation isShowing={isShowing} hide={toggle} config={config}/>
            <button className="btn btn-clean btn-sm" onClick={()=> onEdit()}>
                <i className="fa fa-pencil grey"></i>
            </button>
            <button className="btn btn-clean btn-sm" onClick={onDelete}>
                <i className="fa fa-trash grey"></i>
            </button>

        </div>

    )
};

function UserList(props) {
    const defaultCols = [
        {
            headerName: '',
            field: 'id',
            minWidth: 180,
            headerCheckboxSelection: true,
            headerCheckboxSelectionFilteredOnly: true,
            checkboxSelection: true,
        },
        {
            headerName: "First Name",
            field: 'firstName'
        },
        {
            headerName: "Last Name",
            field: 'lastName'
        },
        {
            headerName: "Email",
            field: 'email'
        },
        {
            headerName: "Description",
            field: 'description'
        },
        {
            headerName: "Actions",
            cellRenderer: ActionsCellRenderer
        }

    ];
    const [rowData, setRowData] = useState([]);
    const [rowSelection, setRowSelection] = useState('multiple');
    const [columnDefs, setColumnDefs] = useState([]);
    const {t, i18n} = useTranslation();


    //function
    const methodFromParent = (rowData)=>{
        setRowData(rowData);
    };

    const onGridReady = (params)=>{
        console.log(params);
    };

    useEffect(()=>{
        getUserList().then(response=>{
            const data = response.data;
            setRowData(data);
        });
        setColumnDefs(defaultCols);
    },[]);






    return (

        <div className="p-3">
            {/*Modal*/}
            {/*<ModalConfirmation isShowing={cellData.isShowing} hide={cellData.toggle}/>*/}
            <div className="card">
                <div className="card-header d-flex justify-content-between border-0 pb-0">
                    <div className="card-title">
                        <h6>User List</h6>
                        <span className="text-muted">User details are being managed</span>
                    </div>
                    <div className="card-toolbar">
                        <div className="dropdown d-inline mx-2">
                            <button className="btn btn-primary dropdown-toggle" role="button"
                               id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                <i className="fa fa-download me-2"></i>
                                <span>{i18n.t("common_export")}</span>
                            </button>
                            <ul className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <button className="dropdown-item">
                                    <i className="fa fa-file-excel-o me-2"></i>
                                    Excel
                                </button>
                                <button className="dropdown-item">
                                    <i className="fa fa-file-text-o me-2"></i>
                                    CSV
                                </button>
                                <button className="dropdown-item">
                                    <i className="fa fa-file-pdf-o me-2"></i>
                                    PDF
                                </button>


                            </ul>
                        </div>
                        <NavLink className="btn btn-primary" to="/users/new">
                            <i className="fa fa-plus me-2"></i>
                            {i18n.t("user_create")}
                        </NavLink>
                    </div>
                </div>
                <div className="card-body">
                    <AgGridTable
                        rowData={rowData}
                        columnDefs={columnDefs}
                        onGridReady={onGridReady}
                        rowSelection={rowSelection}
                        suppressRowClickSelection={true}
                        context={{methodFromParent}}
                    />
                </div>

            </div>

        </div>
    )

}

export default UserList;