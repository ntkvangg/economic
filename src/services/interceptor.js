import axios from "axios";
import React from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const instance = axios.create({
    baseURL: "https://60927c6385ff510017212eac.mockapi.io",
    timeout: 30000
});

instance.interceptors.request.use((req)=>{
    return req;
}, error => {
    return Promise.reject(error);
});

instance.interceptors.response.use(response=>{
    return Promise.resolve(response);
}, error => {
    switch (error?.response?.status) {
        case 404:
            toast.error('Page Not found!');
            break;
        case 409:
            toast.error("Conflict");
            break;
        case 401:
            toast.error("Authentication. Please try again");
            break;
        case 403:
            toast.error("Forbidden. Please try again");
            break;
        default:
            toast.error("Internal sever error");
            break;

    }
    return Promise.reject(error);
});

export default instance;
