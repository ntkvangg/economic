import instance from "../services/interceptor";
const url = '/api/users/users';
export const getUserList = () =>{
    return instance.get(url);
};

export const createUser = (request)=>{
    return instance.post(url, request);
};

export const getCurrentUser = (id)=>{
  return instance.get(`${url}/${id}`);
};

export const updateUser = (id,request)=>{
  return instance.put(url+'/'+id, request);
};

export const deleteUser = (id)=>{
    return instance.delete(url+'/'+id);
};

