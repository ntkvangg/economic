import React from "react";
import {AgGridReact} from "ag-grid-react";

export default class AgGridTable extends React.Component{

    constructor(props){
        super(props);


        this.state = {
            gridApi: null,
            suppressRowClickSelection: this.props.suppressRowClickSelection ? this.props.suppressRowClickSelection : true
        };
    }



    onGridReady = (params) => {
        params.api.sizeColumnsToFit();
        this.props.onGridReady(params);
        this.state.gridApi = params.api;
    };


    render() {
        return (

            <div className="ag-theme-alpine" style={{height: '100%', width: '100%'}}>
                <AgGridReact
                    domLayout={'autoHeight'}
                    animateRows={true}
                    search={this.props.search}
                    rowData={this.props.rowData}
                    columnDefs={this.props.columnDefs}
                    suppressRowClickSelection={this.props.suppressRowClickSelection}
                    suppressMenuHide={false}
                    rowSelection={this.props.rowSelection}
                    pagination={true}
                    paginationPageSize={10}
                    onGridReady={this.onGridReady}
                    context={this.props.context}
                >
                </AgGridReact>
            </div>
        )
    }
}