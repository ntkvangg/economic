import React from 'react';
import {BrowserRouter as Router, Link} from 'react-router-dom';
import "../leftMenu/leftMenu.css";

export default class LeftMenu extends React.Component{
    render() {
        return (

            <nav className="sidenav">
                <Link to="/home" className="list-item">
                    <h5>LOGO</h5>
                </Link>
                <Link to="/users" className="list-item">
                    <i className="fa fa-user-circle me-2">
                    </i>
                    User
                </Link>
            </nav>

        )
    }
}

