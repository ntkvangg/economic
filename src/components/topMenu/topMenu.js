import React from 'react';
import i18n from "i18next";
import "../../components/topMenu/topMenu.css";
export default class TopMenu extends React.Component{
    constructor(props) {
        super(props);
        const languages = [
            {
                code: 'en',
                name:'English',
                countyCode:'us'
            },
            {
                code: 'vn',
                name:'Vietnamese',
                countyCode:'vn'
            }
        ];
        const localLanguage = localStorage.getItem("i18nextLng") || 'en';
        //state
        this.state = {
            languages: [
                {
                    code: 'en',
                    name:'English',
                    countyCode:'us'
                },
                {
                    code: 'vn',
                    name:'Vietnamese',
                    countyCode:'vn'
                }
            ],
            currentLanguage:  languages.find(item=>{return item.code === localLanguage})
        }
    }

    handleChangeLanguage = async (item)=>{
        this.setState({currentLanguage: item});
        await i18n.changeLanguage(item.code);
    };




    render() {


        const languages = [
            {
                code: 'en',
                name:'English',
                countyCode:'us'
            },
            {
                code: 'vn',
                name:'Vietnamese',
                countyCode:'vn'
            }
        ];
        return(
            <div className="topMenu">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="#">Navbar</a>
                        <div className="d-flex">
                            <div className="dropdown">
                                <a className="btn btn-language" role="button"
                                   id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                    <span className={`flag-icon flag-icon-${this.state.currentLanguage.countyCode}`}></span>
                                </a>

                                <ul className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    {languages.map(item=>{
                                        return (
                                            <button className={`dropdown-item ${item.code === this.state.currentLanguage.code ? 'active' : ''}`} onClick={this.handleChangeLanguage.bind(this, item)} key={item.code}>
                                                <span className={`flag-icon flag-icon-${item.countyCode} flag-icon-squared mx-2 international`}></span>
                                                <span>{item.name}</span>
                                            </button>
                                        )
                                    })}
                                </ul>
                            </div>

                            {/*account*/}
                        </div>
                    </div>
                </nav>
            </div>
        )
    }
}