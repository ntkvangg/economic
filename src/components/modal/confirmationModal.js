import React, {useEffect, useState} from 'react';
import {Modal, Button} from "react-bootstrap";
import {Trans, useTranslation} from "react-i18next";

function ModalConfirmation({isShowing, hide, config}) {

    const onPerform = ()=>{
      hide();
      config.onClick();
    };

    const {t, i18n} = useTranslation();
    const name = config.variableI18n;
    return(
        <Modal show={isShowing} onHide={hide}>
        <Modal.Header closeButton>
            <Modal.Title>{config.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Trans i18nKey={config.message}>
                <strong>{{name}}</strong>
            </Trans>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={hide}>
                {i18n.t("common_cancel")}
            </Button>
            <Button variant="danger" onClick={onPerform}>
                {i18n.t("common_delete")}
            </Button>
        </Modal.Footer>
    </Modal>
    )
}

export default ModalConfirmation;