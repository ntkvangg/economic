import React, {useEffect, useState} from "react";
import $ from "jquery";

import "../toast/toast.css";

function Toast(props){

    useEffect(()=>{
        setTimeout(()=>{
           $('.toast').addClass('hide');
           $('.toast').removeClass('show');
        }, 5000)

    });


    const backgroundColorStyle = ()=>{
        switch (props.type) {
            case "success":
                return 'toast-success';
            case "error":
                return "toast-danger";
            case "warning":
                return "toast-warning";
            case "info":
                return "toast-info";
            default:
                return "toast-light";
        }

    };

    const renderIcon = ()=>{
        if (props.type === "success") {
            return 'fa-check-circle-o';
        } else {
            return "fa-exclamation-circle";
        }
    };
    return(
        <div className="position-fixed top-0 end-0 p-2 toast-container">
            <div id="liveToast" className={`toast ${props.show ? 'show' : 'hide' } ${backgroundColorStyle()} p-1`} role="alert" aria-live="assertive" aria-atomic="true">
                <div className="d-flex toast-body">
                    <i className={`${renderIcon()} fa me-2 toast-icon`}></i>
                    <span>{props.message}</span>
                    <button type="button" className="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
            </div>
        </div>

    )
}
export default React.memo(Toast);