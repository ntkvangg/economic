import React from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import {getRoutes} from "../../route/routes";
import "../main/main.css";

export default class Main extends React.Component{
    render() {
        const routes = getRoutes();
        return(
            <div className="mainPage">
                <Routes>
                    {
                        routes.map((item)=>{
                            return (
                                <Route path={item.path} element={item.component} key={item.name}>

                                </Route>
                            )
                        })
                    }
                </Routes>
            </div>
        )
    }
}