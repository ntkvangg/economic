import './App.css';
import React from "react";
import {BrowserRouter as Router} from "react-router-dom";
import TopMenu from "../src/components/topMenu/topMenu";
import LeftMenu from "../src/components/leftMenu/leftMenu";
import Main from "../src/components/main/main";
import { ToastContainer } from 'react-toastify';

function App() {

    return (
        <div className="App">
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme={"colored"}
            />
            <TopMenu/>
            <Router>
                <LeftMenu/>
                <Main/>
            </Router>
        </div>
  );
}

export default App;
