import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
// don't want to use this?
// have a look at the Quick start guide
// for passing in lng and translations on init

i18n
    .use(Backend)
    .use(LanguageDetector)
    // pass the i18n instance to react-i18next.
    .use(initReactI18next)
    .init({
        supportedLngs: ['en', 'vn'],
        fallbackLng: "en",
        debug: true,
        detection: {
            order: ['querystring', 'cookie', 'localStorage', 'sessionStorage', 'navigator', 'htmlTag', 'path', 'subdomain'],
            // cache user language on
            caches: ['localStorage', 'cookie'],
        },
        backend: {
            loadPath: '/assets/locales/{{lng}}.json',
        },
        react: {
            // ...
            hashTransKey: function(defaultValue) {
                // return a key based on defaultValue or if you prefer to just remind you should set a key return false and throw an error
                console.log(defaultValue);
            },
            defaultTransParent: 'div', // a valid react element - required before react 16
            transEmptyNodeValue: '', // what to return for empty Trans
            transSupportBasicHtmlNodes: true, // allow <br/> and simple html elements in translations
            transKeepBasicHtmlNodesFor: ['br', 'strong', 'i'], // don't convert to <1></1> if simple react elements
            transWrapTextNodes: '', // Wrap text nodes in a user-specified element.
                                    // i.e. set it to 'span'. By default, text nodes are not wrapped.
                                    // Can be used to work around a well-known Google Translate issue with React apps. See: https://github.com/facebook/react/issues/11538
                                    // (v11.10.0)
        }
    });

console.log(i18n);
export default i18n;